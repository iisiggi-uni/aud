module Misc where

data WhitespacePosition = UpperLeft | LowerLeft | UpperRight | LowerRight

orderDesc :: Ord a => [a] -> [a]
orderDesc []     = []
orderDesc (p:xs) = orderDesc lesser ++ [p] ++ orderDesc greater
    where
        lesser  = filter (> p) xs
        greater = filter (<= p) xs

        
orderAsc :: Ord a => [a] -> [a]
orderAsc []     = []
orderAsc (p:xs) = orderAsc lesser ++ [p] ++ orderAsc greater
    where
        lesser  = filter (< p) xs
        greater = filter (>= p) xs