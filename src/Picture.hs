module Picture where
import qualified Misc

--aufgabe 1
newtype Picture = Picture {pixels:: [Pixel]}

instance Show Picture where
    show (Picture x) = show (pictureToString (Picture x))


data Pixel = Pixel {position:: Koordinate, farbe:: Char}

instance Eq Pixel where
    Pixel ka fa == Pixel kb fb = ka == kb && fa == fb

instance Ord Pixel where
    compare (Pixel pa _) (Pixel pb _) = compare pa pb

instance Show Pixel where
    show (Pixel _ farbe) = show farbe


data Koordinate = Koordinate {x:: Int, y:: Int} deriving (Show)

instance Eq Koordinate where
    (Koordinate xa ya) == (Koordinate xb yb) = xa == xb && ya == yb

instance Ord Koordinate where
  compare (Koordinate xa ya) (Koordinate xb yb) = if ya == yb then compare xa xb else compare ya yb


data Row = Row {index:: Int, content:: [Pixel]}
type Column = Row

instance Eq Row where
    Row ya pa == Row yb pb = ya == yb && pa == pb

instance Ord Row where
    compare (Row pa _) (Row pb _) = compare pa pb

instance Show Row where
    show (Row i c) = show (i, map farbe c)


--aufgabe 2
flipHorizontal:: Picture -> Picture
flipHorizontal picture = do
    let maxY = getMaxY picture
    convertRowsToPicture (map (\row -> setRowIndex row (1 + maxY - index row)) (getRows picture))

flipVertical:: Picture -> Picture
flipVertical picture = do
    let maxX = getMaxX picture
    convertRowsToPicture (map (\col -> setColumnIndex col (1 + maxX - index col)) (getColumns picture))


--aufgabe 5
printPicture:: Picture -> IO ()
printPicture picture = putStr (pictureToString picture)


--aufgabe 6
invert:: Picture -> Picture
invert picture = Picture (map color (pixels picture))
    where
        color oldPixel
            | farbe oldPixel == '#' = Pixel (position oldPixel) '.'
            | farbe oldPixel == '.' = Pixel (position oldPixel) '#'
            | otherwise = oldPixel


--Set a pixel in a picture
set:: Picture -> Koordinate -> Char -> Picture
set p k c = Picture (help (pixels p) k c)
    where
        help:: [Pixel] -> Koordinate -> Char -> [Pixel]
        help [] _ _ = []
        help (p: ps) koordinate wert = processPixel : help ps koordinate wert
            where
                processPixel
                    | position p == koordinate = Pixel koordinate wert
                    | otherwise = p


get:: Picture -> Koordinate -> Pixel
get p k = help (pixels p) k
    where
        help:: [Pixel] -> Koordinate -> Pixel
        help [] _ = error "Index out of range"
        help (p: ps) k = if position p == k then p else get (Picture ps) k


-- aufgabe 3
placeBeside:: Picture -> Picture -> Picture
placeBeside left right = do
    let maxX = getMaxX left
    addWhitespace (Picture (pixels left ++ pixels (convertRowsToPicture (map (\column -> setColumnIndex column (maxX + index column)) (getColumns right)))))

placeAbove:: Picture -> Picture -> Picture
placeAbove down up = do
    let maxY = getMaxY up
    addWhitespace (Picture (pixels up ++ pixels (convertRowsToPicture (map (\row -> setRowIndex row (maxY + index row)) (getRows down)))))

placeBelow:: Picture -> Picture -> Picture
placeBelow up down = placeAbove down up

repeatHorizontal:: Picture -> Picture
repeatHorizontal picture = placeBelow picture picture

repeatVertical:: Picture -> Picture
repeatVertical picture = placeBeside picture picture

addWhitespace:: Picture -> Picture
addWhitespace picture = do
    --create a new blank picture wit the same dimensions
    let maxX = getMaxX picture
    let maxY = getMaxY picture
    Picture (merge [Pixel (Koordinate x y) f | x <- [1..maxX], y <- [1..maxY], f <- [' ']] (pixels picture))
    where
        -- merge the blank picture with the filled one
        merge:: [Pixel] -> [Pixel] -> [Pixel]
        merge blank [] = blank
        merge blank (p: ps) = merge (filter (\pix -> position pix /= position p) blank ++ [p]) ps










getMaxX:: Picture -> Int
getMaxX p  
    | not (null (pixels p)) = head (Misc.orderDesc (map (x.position) (pixels p)))
    | otherwise = 0

getMaxY:: Picture -> Int
getMaxY p 
    | not (null (pixels p)) = y (position (head (Misc.orderDesc (pixels p))))
    | otherwise = 0


getRows:: Picture -> [Row]
getRows picture = do
    let maxY = getMaxY picture
    [getRow picture row | row <- [1..maxY]]


getRow:: Picture -> Int -> Row
getRow p i = Row i (filter (\px -> y (position px) == i) (pixels p))


setRowIndex:: Row -> Int -> Row
setRowIndex r i = Row i (map (\re -> Pixel (Koordinate (x (position re)) i) (farbe re)) (content r))


getColumns:: Picture -> [Row]
getColumns picture = do
    let maxX = getMaxX picture
    [getColumn picture row | row <- [1..maxX]]


getColumn:: Picture -> Int -> Row
getColumn p i = Row i (filter (\px -> x (position px) == i) (pixels p))


setColumnIndex:: Row -> Int -> Row
setColumnIndex r i = Row i (map (\re -> Pixel (Koordinate i (y (position re))) (farbe re)) (content r))


convertRowsToPicture:: [Row] -> Picture
convertRowsToPicture r = Picture (help r)
    where
        help:: [Row] -> [Pixel]
        help [] = []
        help (r: rs) = content r ++ help rs


convertColumnsToPicture:: [Row] -> Picture
convertColumnsToPicture r = Picture (help r)
    where
        help:: [Row] -> [Pixel]
        help [] = []
        help (r: rs) = content r ++ help rs



pictureToString:: Picture -> String
pictureToString picture = createString (Misc.orderAsc (getRows picture))
    where
        createString:: [Row] -> String
        createString [] = []
        createString (r: rs) = map farbe (Misc.orderAsc (content r)) ++ "\n" ++ createString rs
