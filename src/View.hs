module View where
import qualified Picture as Pic

main:: IO ()
main = do
  picture <- runFileDialog
  Pic.printPicture picture
  loop picture
  where
    loop:: Pic.Picture -> IO ()
    loop picture = do
      alteredPicture <- runActionDialog picture
      Pic.printPicture alteredPicture
      loop alteredPicture


runActionDialog:: Pic.Picture -> IO Pic.Picture
runActionDialog picture = do
  action <- getUserInput "Actions:\ninvert (i), place below (pl), place above (pa), place beside (pb), flip vertical (fv), flip horizontal (fh):"
  execute picture action
  where
    execute:: Pic.Picture -> String -> IO Pic.Picture
    execute picture action = do
      if action == "i"
        then return (Pic.invert picture)
        else if action == "pl"
        then Pic.placeBelow picture <$> runFileDialog
        else if action == "pa"
          then Pic.placeAbove picture <$> runFileDialog
          else if action == "pb"
            then Pic.placeBeside picture <$> runFileDialog
            else if action == "fv"
              then return (Pic.flipVertical picture)
              else if action == "fh"
                then return (Pic.flipHorizontal picture)
                else return picture


runFileDialog:: IO Pic.Picture
runFileDialog = do
  path <- getUserInput "Path to Pic.Picture:"
  importPictureFromFile path

getUserInput:: String -> IO String
getUserInput prompt = do
    putStrLn prompt
    getLine


importPictureFromFile:: String -> IO Pic.Picture
importPictureFromFile path = do
    x <- readFile path
    return (stringToPicture (lines x))
    where
        stringToPicture:: [String] -> Pic.Picture
        stringToPicture [] = Pic.Picture []
        stringToPicture (x: xs) = Pic.placeBelow (stringToLine x) (stringToPicture xs)

        stringToLine:: String -> Pic.Picture
        stringToLine c = Pic.Picture (stringToPixels c 1)

        stringToPixels:: String -> Int -> [Pic.Pixel]
        stringToPixels [] _ = []
        stringToPixels (pixel: xs) index = Pic.Pixel (Pic.Koordinate index 1) pixel: stringToPixels xs (index + 1)
